rs.initiate({
    _id : "myrs",
    members : [
        {
            _id : 0,
            host : "docker.host:27101"
        },
        {
            _id : 1,
            host : "docker.host:27102"
        },
        {
            _id : 2,
            host : "docker.host:27103"
        }
    ]
}, { force: true });

rs.status()

// db.createUser({
//     user: 'dev',
//     pwd: 'secret',
//     roles: [{ role: 'readWrite', db:'apart_deal_api'}]
// })