
#
# db
#

db-stop:
	docker-compose stop mongodb1

db-restart:
	docker-compose stop mongodb1
	docker-compose up -d mongodb1

db-up:
	docker-compose up -d mongodb1

db-reset:
	docker-compose stop mongodb1
	docker-compose rm -f mongodb1
	rm -rf ./disks/mongo1
	docker-compose up -d mongodb1

db-logs:
	docker-compose logs -f mongodb1

db-setup:
	docker-compose up mongosetup

mongosh:
	docker run -it --rm --net=host mongo:5.0.10 mongosh mongodb://127.0.0.1:27102

#
# mongo tls
#

generate-mongo-tls-ca:
	openssl genrsa -out ./mongo/ca.key 4096
	openssl req -new -x509 -days 3650 -key ./mongo/ca.key -out ./mongo/ca.crt -subj "/C=UA/ST=Kyiv/L=Kyiv/O=ApartDeal/OU=Dev/CN=myca" -addext "subjectAltName = IP:127.0.0.1"
	cat ./mongo/ca.crt ./mongo/ca.key > ./mongo/ca.pem

generate-mongo-tls-server-key:
	openssl genrsa -out ./mongo/1/server.key 4096
	openssl genrsa -out ./mongo/2/server.key 4096
	openssl genrsa -out ./mongo/3/server.key 4096

generate-mongo-tls-server-csr:
	openssl req -new -key ./mongo/1/server.key -out ./mongo/1/server.csr -subj "/C=UA/ST=Kyiv/L=Kyiv/O=ApartDeal/OU=Dev/CN=mongodb1" -addext "subjectAltName = IP:127.0.0.1"
	openssl req -new -key ./mongo/2/server.key -out ./mongo/2/server.csr -subj "/C=UA/ST=Kyiv/L=Kyiv/O=ApartDeal/OU=Dev/CN=mongodb2" -addext "subjectAltName = IP:127.0.0.1"
	openssl req -new -key ./mongo/3/server.key -out ./mongo/3/server.csr -subj "/C=UA/ST=Kyiv/L=Kyiv/O=ApartDeal/OU=Dev/CN=mongodb3" -addext "subjectAltName = IP:127.0.0.1"

generate-mongo-tls-server-crt:
	openssl x509 -sha256 -req -days 365 -in ./mongo/1/server.csr -CA ./mongo/ca.crt -CAkey ./mongo/ca.key -CAcreateserial -out ./mongo/1/server.crt -extensions v3_req
	openssl x509 -sha256 -req -days 365 -in ./mongo/2/server.csr -CA ./mongo/ca.crt -CAkey ./mongo/ca.key -CAcreateserial -out ./mongo/2/server.crt -extensions v3_req
	openssl x509 -sha256 -req -days 365 -in ./mongo/3/server.csr -CA ./mongo/ca.crt -CAkey ./mongo/ca.key -CAcreateserial -out ./mongo/3/server.crt -extensions v3_req
	cat ./mongo/1/server.crt ./mongo/1/server.key > ./mongo/1/server.pem
	cat ./mongo/2/server.crt ./mongo/2/server.key > ./mongo/2/server.pem
	cat ./mongo/3/server.crt ./mongo/3/server.key > ./mongo/3/server.pem

generate-mongo-tls-client:
	openssl genrsa -out ./mongo/client1/client.key 4096
	openssl req -new -key ./mongo/client1/client.key -out ./mongo/client1/client.csr -subj "/C=UA/ST=Kyiv/L=Kyiv/O=ApartDeal/OU=DevClient/CN=127.0.0.1"
	openssl x509 -sha256 -req -days 365 -in ./mongo/client1/client.csr -CA ./mongo/ca.crt -CAkey ./mongo/ca.key -CAcreateserial -out ./mongo/client1/client.crt -extensions v3_req
	cat ./mongo/client1/client.crt ./mongo/client1/client.key > ./mongo/client1/client.pem

generate-mongo-tls-server: generate-mongo-tls-server-key generate-mongo-tls-server-csr generate-mongo-tls-server-crt
